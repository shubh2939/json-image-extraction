import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class MainCode {
    public static void main(String[] args) throws IOException {

        String url = "https://apinew.moglix.com/nodeApi/v1/category/getcategory?type=m&abt=n&onlineab=y&orderBy=popularity&orderWay=desc&pageIndex=0&pageSize=40&category=214191500";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        //print in String
        System.out.println(response.toString());
        //Read JSON response and print
        JSONObject myResponse = new JSONObject(response.toString());
        System.out.println(myResponse.getJSONObject("productSearchResult"));
        JSONObject jsonObject = myResponse.getJSONObject("productSearchResult");

        JSONArray jsonArray = (JSONArray) jsonObject.getJSONArray("products");
        int i = 0;
        try {
            i = 0;
            while (!jsonArray.isEmpty()) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                System.out.println(jsonObject1.getString("moglixPartNumber"));
                System.out.println(jsonObject1.getString("mainImageLink"));
                //creating file
                File files=new File("C:\\Users\\shubham.s\\Downloads\\ProductFolder\\"+jsonObject1.getString("moglixPartNumber"));
                boolean created=files.mkdirs();

                System.out.println("folder created");
                //image downloading
                String imageUrl = "https://cdn.moglix.com/p/"+jsonObject1.getString("mainImageLink").replaceAll("p/","");
                System.out.println("image downloaded");
                String destinationFile = "C:\\Users\\shubham.s\\Downloads\\ProductFolder\\"+ jsonObject1.getString("moglixPartNumber")+"\\"+jsonObject1.getString("mainImagePath").replaceAll("/","");
                saveImage(imageUrl, destinationFile);
                i++;
            }
        } catch (JSONException jsonException) {
            System.out.println(i + "files extracted");
        }
    }
    public static void saveImage(String imageUrl, String destinationFile) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);
        byte[] b = new byte[2048];
        int length;
        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }
        is.close();
        os.close();
    }
}